
var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
/* var sass        = require('gulp-sass'); */

//only css
gulp.task('serve', function() {

    browserSync.init({
        server: "./"
    });

    gulp.watch("css/*.css").on('change', browserSync.reload);
    gulp.watch("*.html").on('change', browserSync.reload);
});

gulp.task('default', ['serve']);
